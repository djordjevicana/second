INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'ad_up_tech'                                       AS partner_name,
  SUM(revenue)                                       AS commission,
  'booking'                                          AS date_type,
  'Display Advertising'                AS partner_type
FROM financial.ad_up_tech
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  date,
  website_name,
  'affilinet'                                                             AS partner_name,
  SUM(COALESCE(total_commission, 0) + COALESCE(total_open_commission, 0)) AS commission,
  'booking'                                                               AS date_type,
  'Affiliates'                                                            AS partner_type
FROM financial.affilinet_data
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD')    AS date,
  CASE 
    WHEN region = 'DE' THEN 'UP.de'
    WHEN region = 'FR' THEN 'VP.fr'
    WHEN region = 'GB' THEN 'HP.com' 
    WHEN region = 'ES' THEN 'VP.es'
    WHEN region = 'IT' THEN 'PV.it'
    WHEN region = 'US' THEN 'TP.com'
    ELSE website_name
  END
  as website_name,
  'awin_expedia'                                                          AS partner_name,
  SUM(COALESCE(confirmed_comm_eur, 0) + COALESCE(pending_comm_eur, 0))    AS commission,
  'booking'                                                               AS date_type,
  'Affiliates'                                                            AS partner_type
FROM financial.awin_data
WHERE advertiser_name LIKE '%Expedia%'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD')    AS date,
  CASE 
    WHEN region = 'DE' THEN 'UP.de'
    WHEN region = 'FR' THEN 'VP.fr'
    WHEN region = 'GB' THEN 'HP.com' 
    WHEN region = 'ES' THEN 'VP.es'
    WHEN region = 'IT' THEN 'PV.it'
    WHEN region = 'US' THEN 'TP.com'
    ELSE website_name
  END
  as website_name,
  'awin_rest'                                                             AS partner_name,
  SUM(COALESCE(confirmed_comm_eur, 0) + COALESCE(pending_comm_eur, 0))    AS commission,
  'booking'                                                               AS date_type,
  'Affiliates'                                                            AS partner_type
FROM financial.awin_data
WHERE advertiser_name NOT LIKE '%Expedia%'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'finance_ads'                                      AS partner_name,
  SUM(commission_open + commission_confirmed)        AS commission,
  'booking'                                          AS date_type,
  'Affiliates'                     AS partner_type
FROM financial.financial_ads
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'groupon'                                          AS partner_name,
  SUM(total_commission_amount_eur)                   AS commission,
  'booking'                                          AS date_type,
  'Affiliates'                     AS partner_type
FROM financial.groupon
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'holiday_hypermarket'                                      AS partner_name,
  SUM(commission_eur)                                        AS commission,
  'booking'                                                  AS date_type,
  'IBE'                            AS partner_type
FROM financial.holiday_hypermarket
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'holidu'                                           AS partner_name,
  SUM(total_commission)                              AS commission,
  'booking'                                          AS date_type,
  'Whitelabel'                     AS partner_type
FROM financial.holidu
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'impact_radius'                                    AS partner_name,
  SUM(action_earnings)                               AS commission,
  'booking'                                          AS date_type,
  'Affiliates'                     AS partner_type
FROM financial.impact_radius_subid
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'kayak'                                            AS partner_name,
  CASE 
    WHEN date >= '2018-01-01'
        THEN CASE website_name
    WHEN 'UP.de'   THEN SUM(revenue_eur * 0.7 * 0.70)*0.94
    WHEN 'FP.ch'   THEN SUM(revenue_eur * 0.7 * 0.66)
        WHEN 'HP.com'  THEN SUM(revenue_eur * 0.7 * 0.74)
        WHEN 'PV.it'   THEN SUM(revenue_eur * 0.7 * 0.70)
        WHEN 'TP.com'  THEN SUM(revenue_eur * 0.7 * 0.82)
        WHEN 'VP.es'   THEN SUM(revenue_eur * 0.7 * 0.68)
        WHEN 'VP.fr'   THEN SUM(revenue_eur * 0.7 * 0.84)
        WHEN 'VP.nl'   THEN SUM(revenue_eur * 0.7 * 0.76)
        WHEN 'WP.pl'   THEN SUM(revenue_eur * 0.7 * 0.75)
    END
    ELSE 
      CASE website_name
    WHEN 'UP.de'  THEN SUM(revenue_eur * 0.7 * 0.85)*0.94
        ELSE SUM(revenue_eur * 0.7 * 0.85)
      END
    END AS commission,
    'booking'                                          AS date_type,
    'Kayak'                        AS partner_type
FROM financial.kayak
GROUP BY 1, 2, 3, 5, date
);
INSERT INTO tmp_financial_overview(
SELECT 
  date,
  'UP.at' as website_name,
  partner_name,
  CASE WHEN date >= '2018-01-01'
    THEN sum(commission * 0.7 * 0.7)*0.06 
    ELSE sum(commission * 0.7 * 0.85)*0.06 
  END as comission,
  date_type,
  'Kayak' AS partner_type
FROM (
  SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'kayak'                                            AS partner_name,
  'booking'                                          AS date_type,
  revenue_eur                                        AS commission
FROM financial.kayak
WHERE website_name = 'UP.de')
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(case_creation_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travel_planet'                                                  AS partner_name,
  SUM(travel_planet.reservation_price_eur * 0.035)                 AS commission,
  'booking'                                                        AS date_type,
  'IBE'                                AS partner_type
FROM financial.travel_planet
WHERE reservation_status = 'finalized'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(finalization_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travel_planet'                                                 AS partner_name,
  SUM(travel_planet.reservation_price_eur * 0.035)                 AS commission,
  'travel'                                                        AS date_type,
  'IBE'   AS partner_type
FROM financial.travel_planet
WHERE reservation_status = 'finalized'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(finalization_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travel_planet'                                                  AS partner_name,
  SUM(travel_planet.reservation_price_eur * 0.035)                 AS commission,
  'travel'                                                         AS date_type,
  'IBE'                                AS partner_type
FROM financial.travel_planet
WHERE reservation_status = 'finalized'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travelpartner'                                            AS partner_name,
  SUM(commission)                                            AS commission,
  'booking'                                                  AS date_type,
  'Whitelabel'                         AS partner_type
FROM financial.travelpartner
GROUP BY 1, 2, 3, 5
  UNION
 
SELECT
  to_date(to_char(period, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'webgains'                                           AS partner_name,
  SUM(commision)                                       AS commission,
  'booking'                                            AS date_type,
  'Affiliates'                       AS partner_type
FROM financial.webgains
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
    to_date(to_char(booking_date_time, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
    website_name,
    'ibe'     AS partner_name,
    SUM(price * 0.045) AS commission,
    'booking' AS date_type,
    'IBE'     AS partner_type
  FROM financial.ibe
  WHERE
    website_name = 'FP.ch' AND
    (booking_comment LIKE 'BI best&auml%' OR
    booking_comment = 'Buchung auf BI' OR
    booking_comment = 'Buchung auf Request' OR
    booking_comment = 'gebucht' OR
    booking_comment LIKE 'BI best&auml%')
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(booked, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'booking_com'                                              AS partner_name,
  CASE website_name
  WHEN 'VP.es'  THEN SUM(fee_eur) * 0.35
  WHEN 'UP.at'  THEN SUM(fee_eur) * 0.55
  WHEN 'VP.nl'  THEN SUM(fee_eur) * 0.65
  WHEN 'PV.it'  THEN SUM(fee_eur) * 0.3
  WHEN 'HP.com' THEN SUM(fee_eur) * 0.4
  ELSE SUM(fee_eur) * 0.5
  END AS commission,
  'booking'                                                  AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.booking_com
WHERE status = 'booked' or status = 'stayed'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(departure, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'booking_com'                                              AS partner_name,
  CASE website_name
  WHEN 'VP.es'  THEN SUM(fee_eur) * 0.35
  WHEN 'UP.at'  THEN SUM(fee_eur) * 0.55
  WHEN 'VP.nl'  THEN SUM(fee_eur) * 0.65
  WHEN 'PV.it'  THEN SUM(fee_eur) * 0.3
  WHEN 'HP.com' THEN SUM(fee_eur) * 0.4
  ELSE SUM(fee_eur) * 0.5
  END AS commission,
  'travel'                                                   AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.booking_com
WHERE status = 'booked' or status = 'stayed'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'effiliation'                                              AS partner_name,
  SUM(commission)                                            AS commission,
  'booking'                                                  AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.effiliation
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'google_ads' AS partner_name,
  SUM(ad_exchange_estimated_revenue) AS commision,
  'booking' AS date_type,
  'Display Advertising' AS partner_type
FROM financial.google_ads
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
    t1.date,
    t1.website as website_name,
    t1.partner_name,
    SUM(t1.amount_eur) * MAX(t2.commission_rate) as commission,
    t1.date_type,
    'City Trips'                                 AS partner_type
FROM       
(SELECT
  to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  DATEPART('year',booking_date) as year,
  website_name                          AS website,
  'cityholidays'                        AS partner_name,
  'booking'                             AS date_type,
  SUM(amount_eur)                       AS amount_eur
FROM financial.cityholidays
GROUP BY 1, 2, 3, 4, 5) t1
LEFT JOIN
(SELECT 
  website_name, 
  DATEPART('year',booking_date) as year, 
    CASE WHEN SUM(amount_eur) < 250000
      THEN 0.075
     WHEN SUM(amount_eur) >= 250000 AND SUM(amount_eur) < 500000
      THEN 0.085
    ELSE 0.095
  END AS commission_rate
FROM financial.cityholidays
WHERE DATEPART('year',booking_date) ~ '[0-9]{4}'
GROUP BY 
DATEPART('year',booking_date), 
website_name) t2
on t1.website = t2.website_name AND t1.year = t2.year
WHERE t1.date > '1990-01-01'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(event_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'commission_junction' AS partner_name,
  SUM(commission_amount) AS commision,
  'booking' AS date_type,
  'Affiliates'    AS partner_type
FROM financial.commission_junkie
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  date,
  website_name,
  'daisycoin'                               AS partner_name,
  SUM(transaction_approved_amount + transaction_open_amount)              AS commision,
  'booking'                                   AS date_type,
  'Affiliates'                                AS partner_type
FROM financial.daisycon_daily_stats
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
    to_date(to_char(departure_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
    website_name,
    'ibe'    AS partner_name,
    SUM(price * 0.045) AS commission,
    'travel' AS date_type,
    'IBE'    AS partner_type
  FROM financial.ibe
  WHERE
    website_name = 'FP.ch' AND
    (booking_comment LIKE 'BI best&auml%' OR
    booking_comment = 'Buchung auf BI' OR
    booking_comment = 'Buchung auf Request' OR
    booking_comment = 'gebucht' OR
    booking_comment LIKE 'BI best&auml%')
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  date,
  website_name,
  'ibe'     AS partner_name,
  CASE 
  WHEN date >= '2015-10-01' AND date < '2016-10-31'
    THEN
        CASE tour_operator
        WHEN '5VF'
          THEN SUM(revenue * 0.103)
        WHEN 'AB'
          THEN SUM(revenue * 0.06)
        WHEN 'ALL'
          THEN SUM(revenue * 0.105)
        WHEN 'BCH'
          THEN SUM(revenue * 0.13)
        WHEN 'BIG'
          THEN SUM(revenue * 0.103)
        WHEN 'BYE'
          THEN SUM(revenue * 0.105)
        WHEN 'DAF'
          THEN SUM(revenue * 0.13)
        WHEN 'ECC'
          THEN SUM(revenue * 0.12)  
        WHEN 'FOR'
          THEN SUM(revenue * 0.11)  
        WHEN 'FTI'
          THEN SUM(revenue * 0.103)  
        WHEN 'JT'
          THEN SUM(revenue * 0.12)  
        WHEN 'LMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'MED'
          THEN SUM(revenue * 0.11)  
        WHEN 'MLA'
          THEN SUM(revenue * 0.11)  
        WHEN 'MON'
          THEN SUM(revenue * 0.12)  
        WHEN 'OGE'
          THEN SUM(revenue * 0.11)
        WHEN 'OLI'
          THEN SUM(revenue * 0.115)  
        WHEN 'RIVA'
          THEN SUM(revenue * 0.12)  
        WHEN 'ST'
          THEN SUM(revenue * 0.13)  
        WHEN 'TROP'
          THEN SUM(revenue * 0.12)  
        WHEN 'VTO'
          THEN SUM(revenue * 0.11)  
        WHEN 'VTOI'
          THEN SUM(revenue * 0.11)  
        WHEN 'X5VF'
          THEN SUM(revenue * 0.103)  
        WHEN 'XALL'
          THEN SUM(revenue * 0.105)  
        WHEN 'XBIG'
          THEN SUM(revenue * 0.103)
        WHEN 'XFTI'
          THEN SUM(revenue * 0.103)  
        WHEN 'XLMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'XOGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'XPOD'
          THEN SUM(revenue * 0.12)         
        WHEN 'ATID'
          THEN SUM(revenue * 0.07)          
        WHEN 'FLYD'
          THEN SUM(revenue * 0.07)          
        WHEN 'TUID'
          THEN SUM(revenue * 0.07)          
        WHEN 'XTUI'
          THEN SUM(revenue * 0.07)                   
    ELSE SUM(revenue * 0.1)
        END
  WHEN date >= '2016-11-01' AND date < '2017-11-01'
    THEN
      CASE tour_operator
        WHEN '5VF'
          THEN SUM(revenue * 0.11)
        WHEN 'ALL'
          THEN SUM(revenue * 0.105)
        WHEN 'ATID'
          THEN SUM(revenue * 0.07)
        WHEN 'BCH'
          THEN SUM(revenue * 0.13)
        WHEN 'BIG'
          THEN SUM(revenue * 0.11)
        WHEN 'BYE'
          THEN SUM(revenue * 0.105)
        WHEN 'DER'
          THEN SUM(revenue * 0.105)
        WHEN 'ECC'
          THEN SUM(revenue * 0.12)
        WHEN 'FER'
          THEN SUM(revenue * 0.11)  
        WHEN 'FLYD'
          THEN SUM(revenue * 0.07)  
        WHEN 'FOR'
          THEN SUM(revenue * 0.11)  
        WHEN 'FTI'
          THEN SUM(revenue * 0.11)  
        WHEN 'HHT'
          THEN SUM(revenue * 0.115)  
        WHEN 'ITS'
          THEN SUM(revenue * 0.105)  
        WHEN 'ITSX'
          THEN SUM(revenue * 0.105)  
        WHEN 'JAHN'
          THEN SUM(revenue * 0.105)  
        WHEN 'JT'
          THEN SUM(revenue * 0.12)  
        WHEN 'LMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'MED'
          THEN SUM(revenue * 0.11)  
        WHEN 'MLA'
          THEN SUM(revenue * 0.11)  
        WHEN 'MWR'
          THEN SUM(revenue * 0.105)  
        WHEN 'OGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'OLI'
          THEN SUM(revenue * 0.115)  
        WHEN 'RIVA'
          THEN SUM(revenue * 0.12)  
        WHEN 'TJAX'
          THEN SUM(revenue * 0.105)  
        WHEN 'TROP'
          THEN SUM(revenue * 0.12)  
        WHEN 'TUID'
          THEN SUM(revenue * 0.07)  
        WHEN 'VTO'
          THEN SUM(revenue * 0.11)  
        WHEN 'VTOI'
          THEN SUM(revenue * 0.11)  
        WHEN 'X5VF'
          THEN SUM(revenue * 0.11)  
        WHEN 'XALL'
          THEN SUM(revenue * 0.105)  
        WHEN 'XBIG'
          THEN SUM(revenue * 0.11)  
        WHEN 'XDER'
          THEN SUM(revenue * 0.105)  
        WHEN 'XFTI'
          THEN SUM(revenue * 0.11)  
        WHEN 'XJAH'
          THEN SUM(revenue * 0.105)  
        WHEN 'XLMX'
          THEN SUM(revenue * 0.105)  
        WHEN 'XMWR'
          THEN SUM(revenue * 0.105)  
        WHEN 'XOGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'XPOD'
          THEN SUM(revenue * 0.12)  
        WHEN 'XTUI'
          THEN SUM(revenue * 0.07)  
        ELSE SUM(revenue * 0.1)
        END
    WHEN date >= '2017-01-01'
      THEN
        CASE tour_operator
        WHEN 'FER'
          THEN SUM(revenue * 0.11)
        WHEN 'HHT'
          THEN SUM(revenue * 0.115)  
          ELSE SUM(revenue * 0.1)  
        END
        ELSE SUM(revenue * 0.1)  
      END AS commission,  
      'booking' AS date_type,
      'IBE' AS partner_type
FROM (
  SELECT
    to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
    CASE departure_airport
    WHEN 'BRN'
      THEN 'FP.ch'
    WHEN 'BSL'
      THEN 'FP.ch'
    WHEN 'GVA'
      THEN 'FP.ch'
    WHEN 'ZRH'
      THEN 'FP.ch'
    ELSE website_name
    END AS website_name,
    tour_operator,
    revenue
  FROM financial.midoco
  WHERE
    booking_status = 'OK' OR
    booking_status = 'Buchung auf BI' OR
    booking_status = 'Buchung auf Request' OR
    booking_status = 'gebucht' OR
    booking_status LIKE 'BI best&auml%')
GROUP BY 1, 2, 3, 5, tour_operator
);
INSERT INTO tmp_financial_overview(
SELECT
  date,
  website_name,
  'ibe'    AS partner_name,
  CASE 
  WHEN date >= '2015-10-01' AND date < '2016-10-31'
    THEN
        CASE tour_operator
        WHEN '5VF'
          THEN SUM(revenue * 0.103)
        WHEN 'AB'
          THEN SUM(revenue * 0.06)
        WHEN 'ALL'
          THEN SUM(revenue * 0.105)
        WHEN 'BCH'
          THEN SUM(revenue * 0.13)
        WHEN 'BIG'
          THEN SUM(revenue * 0.103)
        WHEN 'BYE'
          THEN SUM(revenue * 0.105)
        WHEN 'DAF'
          THEN SUM(revenue * 0.13)
        WHEN 'ECC'
          THEN SUM(revenue * 0.12)  
        WHEN 'FOR'
          THEN SUM(revenue * 0.11)  
        WHEN 'FTI'
          THEN SUM(revenue * 0.103)  
        WHEN 'JT'
          THEN SUM(revenue * 0.12)  
        WHEN 'LMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'MED'
          THEN SUM(revenue * 0.11)  
        WHEN 'MLA'
          THEN SUM(revenue * 0.11)  
        WHEN 'MON'
          THEN SUM(revenue * 0.12)  
        WHEN 'OGE'
          THEN SUM(revenue * 0.11)
        WHEN 'OLI'
          THEN SUM(revenue * 0.115)  
        WHEN 'RIVA'
          THEN SUM(revenue * 0.12)  
        WHEN 'ST'
          THEN SUM(revenue * 0.13)  
        WHEN 'TROP'
          THEN SUM(revenue * 0.12)  
        WHEN 'VTO'
          THEN SUM(revenue * 0.11)  
        WHEN 'VTOI'
          THEN SUM(revenue * 0.11)  
        WHEN 'X5VF'
          THEN SUM(revenue * 0.103)  
        WHEN 'XALL'
          THEN SUM(revenue * 0.105)  
        WHEN 'XBIG'
          THEN SUM(revenue * 0.103)
        WHEN 'XFTI'
          THEN SUM(revenue * 0.103)  
        WHEN 'XLMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'XOGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'XPOD'
          THEN SUM(revenue * 0.12)         
        WHEN 'ATID'
          THEN SUM(revenue * 0.07)          
        WHEN 'FLYD'
          THEN SUM(revenue * 0.07)          
        WHEN 'TUID'
          THEN SUM(revenue * 0.07)          
        WHEN 'XTUI'
          THEN SUM(revenue * 0.07)                   
        ELSE SUM(revenue * 0.1)
        END
  WHEN date >= '2016-11-01' AND date < '2017-11-01'
    THEN
      CASE tour_operator
        WHEN '5VF'
          THEN SUM(revenue * 0.11)
        WHEN 'ALL'
          THEN SUM(revenue * 0.105)
        WHEN 'ATID'
          THEN SUM(revenue * 0.07)
        WHEN 'BCH'
          THEN SUM(revenue * 0.13)
        WHEN 'BIG'
          THEN SUM(revenue * 0.11)
        WHEN 'BYE'
          THEN SUM(revenue * 0.105)
        WHEN 'DER'
          THEN SUM(revenue * 0.105)
        WHEN 'ECC'
          THEN SUM(revenue * 0.12)
        WHEN 'FER'
          THEN SUM(revenue * 0.11)  
        WHEN 'FLYD'
          THEN SUM(revenue * 0.07)  
        WHEN 'FOR'
          THEN SUM(revenue * 0.11)  
        WHEN 'FTI'
          THEN SUM(revenue * 0.11)  
        WHEN 'HHT'
          THEN SUM(revenue * 0.115)  
        WHEN 'ITS'
          THEN SUM(revenue * 0.105)  
        WHEN 'ITSX'
          THEN SUM(revenue * 0.105)  
        WHEN 'JAHN'
          THEN SUM(revenue * 0.105)  
        WHEN 'JT'
          THEN SUM(revenue * 0.12)  
        WHEN 'LMX'
          THEN SUM(revenue * 0.12)  
        WHEN 'MED'
          THEN SUM(revenue * 0.11)  
        WHEN 'MLA'
          THEN SUM(revenue * 0.11)  
        WHEN 'MWR'
          THEN SUM(revenue * 0.105)  
        WHEN 'OGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'OLI'
          THEN SUM(revenue * 0.115)  
        WHEN 'RIVA'
          THEN SUM(revenue * 0.12)  
        WHEN 'TJAX'
          THEN SUM(revenue * 0.105)  
        WHEN 'TROP'
          THEN SUM(revenue * 0.12)  
        WHEN 'TUID'
          THEN SUM(revenue * 0.07)  
        WHEN 'VTO'
          THEN SUM(revenue * 0.11)  
        WHEN 'VTOI'
          THEN SUM(revenue * 0.11)  
        WHEN 'X5VF'
          THEN SUM(revenue * 0.11)  
        WHEN 'XALL'
          THEN SUM(revenue * 0.105)  
        WHEN 'XBIG'
          THEN SUM(revenue * 0.11)  
        WHEN 'XDER'
          THEN SUM(revenue * 0.105)  
        WHEN 'XFTI'
          THEN SUM(revenue * 0.11)  
        WHEN 'XJAH'
          THEN SUM(revenue * 0.105)  
        WHEN 'XLMX'
          THEN SUM(revenue * 0.105)  
        WHEN 'XMWR'
          THEN SUM(revenue * 0.105)  
        WHEN 'XOGE'
          THEN SUM(revenue * 0.11)  
        WHEN 'XPOD'
          THEN SUM(revenue * 0.12)  
        WHEN 'XTUI'
          THEN SUM(revenue * 0.07)  
        ELSE SUM(revenue * 0.1)
        END
    WHEN date >= '2017-01-01'
      THEN
        CASE tour_operator
        WHEN 'FER'
          THEN SUM(revenue * 0.11)
        WHEN 'HHT'
          THEN SUM(revenue * 0.115)  
    ELSE SUM(revenue * 0.1)  
        END
        ELSE SUM(revenue * 0.1)  
      END AS commission,
  'travel' AS date_type,
  'IBE' AS partner_type 
FROM (
  SELECT
    to_date(to_char(departure_date_start, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
    CASE departure_airport
    WHEN 'BRN'
      THEN 'FP.ch'
    WHEN 'BSL'
      THEN 'FP.ch'
    WHEN 'GVA'
      THEN 'FP.ch'
    WHEN 'ZRH'
      THEN 'FP.ch'
    ELSE website_name
    END AS website_name,
    tour_operator,
    revenue
  FROM financial.midoco
  WHERE
     booking_status = 'OK' OR
    booking_status = 'Buchung auf BI' OR
    booking_status = 'Buchung auf Request' OR
    booking_status = 'gebucht' OR
    booking_status LIKE 'BI best&auml%')
GROUP BY 1, 2, 3, 5, tour_operator
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  'WP.pl'                          AS website_name,
  'fostertravel'                                             AS partner_name,
  SUM(commission * 0.23)                                     AS commission,
  'booking'                                                  AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.fostertravel
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT 
    b.date,
    CASE 
        WHEN website_name is null THEN website 
        ELSE website_name 
    END as website_name,
    CASE 
        WHEN partner_name is null THEN 'direct_partner'
        ELSE partner_name
    END as partner_name,
    CASE
        WHEN commission is null THEN 0 
        ELSE commission
    END as commission,
    CASE
        WHEN date_type is null THEN 'booking'
        ELSE date_type
    END as date_type,   
    'Direct Partner' AS partner_type
FROM
(SELECT
  campaign_date,
  website_name,
  'direct_partner' AS partner_name,
  'booking'        AS date_type,
  SUM(invoiced_price_eur) as commission
FROM financial.direct_partners
GROUP BY 1, 2, 3, 4) as a
RIGHT JOIN
(SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name as website
FROM financial.kayak
GROUP BY 
to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD'),
website_name 
UNION
SELECT
  DISTINCT(to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD')) AS date,
  'UP.at' as website
FROM financial.kayak
GROUP BY 
to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD'),
website
)as b
on a.campaign_date = b.date and a.website_name = b.website
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'secret_escapes'                                   AS partner_name,
  SUM(commision * 0.5)                               AS commision,
  'booking'                                          AS date_type,
  'Whitelabel'                       AS partner_type
FROM financial.secret_escapes_daily
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(departure_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'holiday_hypermarket'                                        AS partner_name,
  SUM(commission_eur)                                          AS commission,
  'travel'                                                     AS date_type,
  'IBE'                              AS partner_type
FROM financial.holiday_hypermarket
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(start_travel_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travelpartner'                                                 AS partner_name,
  SUM(commission)                                                 AS commission,
  'travel'                                                        AS date_type,
  'Whitelabel'                            AS partner_type
FROM financial.travelpartner
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(finalization_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travel_planet'                                                 AS partner_name,
  SUM(travel_planet.reservation_price_eur * 0.035)                AS commission,
  'travel'                                                        AS date_type,
  'IBE'                                 AS partner_type
FROM financial.travel_planet
WHERE reservation_status = 'finalized'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT 
  to_date(to_char(finalization_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travel_planet' AS partner_name,
  SUM(travel_planet.reservation_price_eur * 0.035) AS commission,
  'travel'  AS date_type,
  'IBE' AS partner_type
FROM financial.travel_planet
WHERE reservation_status = 'finalized'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(booking_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'loveholidays'                                             AS partner_name,
  SUM(approx_margin_eur)                                     AS commission,
  'booking'                                                  AS date_type,
  'IBE'                            AS partner_type
FROM financial.loveholiday
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(booking, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'logitravel'                                               AS partner_name,
  SUM(margin)                                                AS commission,
  'booking'                                                  AS date_type,
  'Whitelabel'                                               AS partner_type
FROM financial.logitravel_booking_details
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(checkout, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'logitravel'                                               AS partner_name,
  SUM(margin)                                                AS commission,
  'travel'                                                   AS date_type,
  'Whitelabel'                                               AS partner_type
FROM financial.logitravel_booking_details
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'travelaudience' AS partner_name,
  SUM(revenue) AS commision,
  'booking' AS date_type,
  'Display Advertising' AS partner_type
FROM financial.travelaudience
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'trivago'                                                  AS partner_name,
  SUM(commission_calc)                                       AS commission,
  'booking'                                                  AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.trivago          
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(sale_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'webgears' AS partner_name,
  SUM(revenue) AS commision,
  'booking' AS date_type,
  'Affiliates'  AS partner_type
FROM financial.webgears
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(sale_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'webgears' AS partner_name,
  SUM(revenue) AS commision,
  'booking' AS date_type,
  'Affiliates'   AS partner_type
FROM financial.webgears
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(click_date, 'YYYY-MM-DD'), 'YYYY-MM-DD')    AS date,
  website_name,
  'zanox'                                                     AS partner_name,
  SUM(commission)                                             AS commission,
  'booking'                                                   AS date_type,
  'Affiliates'                                                AS partner_type
FROM financial.zanox_data
WHERE review_state != 'rejected'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'trivago'                                                  AS partner_name,
  SUM(commission_calc)                                       AS commission,
  'booking'                                                  AS date_type,
  'Affiliates'                                               AS partner_type
FROM financial.trivago
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(reservation_date_time, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  'TP.com'                           AS website_name,
  'priceline'                                                AS partner_name,
  SUM(commission)                                            AS commission,
  'booking'                                                  AS date_type,
  'IBE'                                                      AS partner_type
FROM financial.priceline_hotels
WHERE status = 'Active'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(reservation_date_time, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  'TP.com'                           AS website_name,
  'priceline'                                                AS partner_name,
  SUM(commission)                                            AS commission,
  'booking'                                                  AS date_type,
  'IBE'                                                      AS partner_type
FROM financial.priceline_flights
WHERE status = 'Active'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  'TP.com'                           AS website_name,
  'priceline'                                                AS partner_name,
  SUM(commission)                                            AS commission,
  'booking'                                                  AS date_type,
  'IBE'                                                      AS partner_type
FROM financial.priceline_cars
WHERE status = 'Active'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview( 
SELECT
  to_date(to_char(departure_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') AS date,
  website_name,
  'loveholidays'                                               AS partner_name,
  SUM(approx_margin_eur)                                       AS commission,
  'travel'                                                     AS date_type,
  'IBE'                              AS partner_type
FROM financial.loveholiday
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  date,
  website_name,
  'tradedoubler' AS partner_name,
  SUM(publisher_commission) AS commision,
  'booking' AS date_type,
  'Affiliates'              AS partner_type
FROM financial.tradedoubler
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT
  to_date(to_char(click_date, 'YYYY-MM-DD'), 'YYYY-MM-DD')    AS date,
  website_name,
  'digidip'                                                   AS partner_name,
  SUM(commission)                                             AS commission,
  'booking'                                                   AS date_type,
  'Affiliates'                          AS partner_type
FROM financial.digidip_transactions
WHERE status != 'denied'
GROUP BY 1, 2, 3, 5
);
INSERT INTO tmp_financial_overview(
SELECT    
  to_date(to_char(bk_date, 'YYYY-MM-DD'), 'YYYY-MM-DD')    AS date,
   website_name,
  'lastminute'                                               AS partner_name,
  SUM(CONVERT(INT,hp_commission))                            AS commission,
  'booking'                                                  AS date_type,
  'Whitelabel'
FROM financial.lastminute
WHERE hp_commission != 'tbc'
GROUP BY 1, 2, 3, 5
);